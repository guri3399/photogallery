<?php
	include 'inc/db.inc.php';
	function get_recent_pics()
	{
		$limit = 3;
		$query = "SELECT * FROM pics ORDER BY  pid DESC LIMIT $limit ";

		$queryrun = $GLOBALS['$conn']->query($query);
		if($queryrun -> num_rows > 0) {
			while ($row = $queryrun -> fetch_assoc()) {
				$picname = $row['picname'];
				$pid =  $row['pid'];
				$author = $row['username'];
				$src = 'uploads/' . $author . '/' . $picname ;
				?>
				<div class="col-md-4">
					<div class="gallery-image">
						<img src="<?php echo $src ; ?>" class="front">
						<div class="back">
							<div class="back-content">
								<h3><?php echo $picname;?></h3>
								<h6>
									<em>By</em>
									<?php echo $author; ?>
								</h6>
								<a href="<?php echo $src; ?>" data-lightbox="gallery"><i class="fa fa-expand"></i></a>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}

	}
	function get_home_gallery_content($x)
	{
	// {
	// 	we will show url like page==2 and perpage =10
		
		$approved = 1;
		$query = "SELECT * FROM pics WHERE approved='$approved' LIMIT $x";
		$query_run = $GLOBALS['$conn']->query($query);
		if ($query_run -> num_rows > 0) {
			while ($row = $query_run->fetch_assoc()) 
			{
				$picname = $row['picname'];
				$pid = $row['pid'];
				$author = $row['username'];
				$src = 'uploads/' . $author . '/' .$picname ;
				?>
				<div class="col-md-4">
					<div class="gallery-image">
						<img src="<?php echo $src ;?>" class="front">
						<div class="back">
							<div class="back-content">
								<h3><?php echo $picname;?></h3>
								<h6>
									<em>By</em>
									<?php echo $author; ?>
								</h6>
								<a href="<?php echo $src; ?>" data-lightbox="gallery"><i class="fa fa-expand"></i></a>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
		<script type="text/javascript">
		    lightbox.option({
		      'resizeDuration': 200,
		      'wrapAround': true
		    })
		</script>
		<?php
	}
	function get_gallery_content()
	{
	// {
	// 	we will show url like page==2 and perpage =10
		if (isset($_GET['page'])) 
		{
			$page = (int)$_GET['page'];//parsing as integer
			//if it is set then page variable will be number on url if this is not there then its value will be 1 means we will on first page
		}
		else
		{
			$page = $_GET['page'] = 1 ;
		}
		if (isset($_GET['per_page']) && ($_GET['per_page'])<21) 
			//means we are preventing of user input in perpage=10000 means error handling
		{
			$per_page = $_GET['per_page'];
		}
		else
		{	
			// default will be 6
			$per_page = 3 ;
		}
		$approved = 1;
		$total_query = "SELECT * FROM pics WHERE approved = '$approved'";
		//we are retrieving all the images
		$total = $GLOBALS['$conn']->query($total_query);
		$total = $total->num_rows;
		$pages = ceil($total/$per_page);
		//echo $pages;
		// ceil will be set the value to upper means if 3.soemtihing it will be 4
		//if we are at page 1 start will be 0 or u want to show iamges ofrom 1 to 10 for that index will be 0 to 9 
		$start = ($page*$per_page) - $per_page;
		//echo $start;
		//start is - elnth is 3 0,1,2 and then 3,4,5
		$query = "SELECT * FROM pics WHERE approved='$approved' LIMIT $start,$per_page";
		$query_run = $GLOBALS['$conn']->query($query);
		if ($query_run -> num_rows > 0) {
			while ($row = $query_run->fetch_assoc()) 
			{
				$picname = $row['picname'];
				$pid = $row['pid'];
				$author = $row['username'];
				$src = 'uploads/' . $author . '/' .$picname ;
				?>
				<div class="col-md-4">
					<div class="gallery-image">
						<img src="<?php echo $src ;?>" class="front">
						<div class="back">
							<div class="back-content">
								<h3><?php echo $picname;?></h3>
								<h6>
									<em>By</em>
									<?php echo $author; ?>
								</h6>
								<a href="<?php echo $src; ?>" data-lightbox="gallery"><i class="fa fa-expand"></i></a>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
		<div class="clearfix">
			
		</div>
		<div id="pagination">
			<?php
				for ($i=1; $i <= $pages; $i++)
				{ 
					?>
						<a href="?page=<?php echo$i.'&per_page='.$per_page;?>">Page<?php echo $i;?></a>
					<?php
				}

			?>
		</div>
		<script type="text/javascript">
		    lightbox.option({
		      'resizeDuration': 200,
		      'wrapAround': true
		    })
		</script>
		<?php
	}
	//we are receiveing username from session variable and now we are displaying the values in prpfile info
	function get_profile_info($username)
	{
		$fname="";
		$lname="";
		$email="";
		$bio="";

		$query = "SELECT * from user WHERE username='$username'";
		$queryrun =  $GLOBALS['$conn']->query($query);
		if($queryrun->num_rows > 0)
		{
		        echo "<div class='col-md-2'>
		        	First Name : <br>
		        	Last Name : <br>
		        	Email : <br> 
		        	Bio : <br>
		        	</div>";
			while($row = $queryrun->fetch_assoc()) 
			 {
		        echo "<div class='col-md-4'>";
		        echo $fname = $row['fname'] . '<br>';
		        echo $lname = $row['lname'] . '<br>';
		        echo $email = $row['email'] . '<br>';
		        if ($row['bio']=='') 
		        {
		        	echo "You Did Not Provide Your Bio Yet <br>";
		        }
		        else
		        {
		        	echo $row['bio'];
		        }
    		}

    		echo "</div>";
		} 
		else 
		{
	    echo "0 results";
		}
	}
	//logic we will using file handling But no I am going to reveal our plan will be implementing this using file handling metod.

// whenever a user  is registering in the site and then logging in and coming to this index
//page a folder will be created inside a default folder and the name of that folder will be this username.

// See for example this John welcome John.

// So currently John has been long been.

// So whenever he's logging in for the first time a folder John will be created inside a default folder.

// Well all the users folders will be inside that default folder.

// Now inside each user folder there will be those images that a user is uploading and know how he can

// handle that profile image separately.

// For that we will create another folder inside this Jaun folder.

// Or you can see that username folder that folder name will be set of a door and inside that will be having

// that profile picture.
	//this funciion is used for creating and validating new users's folder

	function get_avatar_image($user)
	{
		$pic = 0;
		$upload_folder = "uploads";
		$user_folder = $upload_folder . '/' . $user ;
		$avatar_image_folder =$user_folder . '/avatar';
		if(is_dir($upload_folder))
		{
			if (is_dir($user_folder))
			{
				
			}	
			else
			{
				mkdir($user_folder);
			}
		}
		else
		{
			mkdir($upload_folder);
			if (is_dir($user_folder))
			{
				
			}	
			else
			{
				mkdir($user_folder);
			}
		}
		if (is_dir($avatar_image_folder)) 
		{
			
		}
		else
		{
			mkdir($avatar_image_folder);
		}
		
		if($handle = opendir($avatar_image_folder))
		{
			while(false!== ($entry = readdir($handle)))
			{
				if(($entry!='.') and ($entry!='..'))
				{
					$pic = 1;
					$avatar_image_path = $avatar_image_folder.'/'.$entry;
					echo "<img src=$avatar_image_path alt=$entry id=avatar-image-id width='300px'/>";
				}
			}

			closedir($handle);
		}
		//if user has just registerd and login intially there will be no image we will have daefalut image in avatar image div
	//we will define a variable pic with intialisaion if 
	// if of entry and entry we will asisign $pic =1;
	//we will check if $pic is 0 then deafult will be there
		if($pic==0)
		{
			echo "<img src='img/user-default.jpg' id='avatar-image-id' width='300px'/>";
		}
	}
	
// The first thing is it will check that it is a image or not going to say it is not of any
// age type then we will check that for the file size is normal or not like what will support say around
// 500 kilobytes.
// You can if you want to can sample bigger size of files but just for simplicity and keeping it normal
// else then we will upload this picture to a lot of other image folder that is inside that folder and 
// automatically label seatback image.
		
		function get_user_uploaded_pics($username)
		{
			// The ORDER BY clause is used to sort the result-set in ascending or descending order.

// The ORDER BY clause sorts the records in ascending order by default. To sort the records in descending order, use the DESC keyword.
			$query = "SELECT * FROM pics WHERE username = '$username' ORDER BY pid DESC ";
		$queryrun =  $GLOBALS['$conn']->query($query);
		if($queryrun->num_rows > 0)
		{
			while ($row = $queryrun->fetch_assoc()) {
				$picid = $row['pid'];
				$picname = $row['picname'];
				$path = 'uploads/'.$username.'/'.$picname;
				?>
				<div class="col-md-4">
					<img src="<?php echo $path;?>">
				</div>
				<?php
			}
		}
	}	
/*function get_unapproved_pics()
	{
		$approved = 0;
		$query = "SELECT * FROM pics WHERE approved = ' $approved'";
		$queryrun =  $GLOBALS['$conn']->query($query);
		if($queryrun->num_rows > 0)
		{
			while ($row = $queryrun->fetch_assoc()) {
				$pid = $row['pid'];
				$picname = $row['picname'];
				$uname = $row['username'];
				$src = 'uploads/'.$uname.'/'.$picname;
				?>
				<div id="row-<?php echo $pid ; ?>">
					<div class="col-md-4">
						<img src="<?php echo $src ;?>" id="<?php echo $pid ; ?>">
					</div>
					<div class="col-md-4">
						<?php echo $picname; ?>
					</div>
					<div class="col-md-4">
						<button  id="Yes-<?php echo $pid;?>;" onclick=approvedimage(<?php echo $pid ;?>)>Yes</button>
						<button id="No-<?php echo $pid;?>;" onclick=deleteimage(<?php echo $pid ;?>)>No</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<?php
			}
		}
	}	*/	
?>