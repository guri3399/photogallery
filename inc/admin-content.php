<?php include 'db.inc.php';?>
<section class="review">
	<div class="section-header center">
		<h1>Welcome <?php echo ucfirst($_SESSION['admin']);?></h1>
	</div>
	<div class="container">
		<div class="row">
			<div id="unapproved_pics">
				<?php get_unapproved_pics(); ?>
				<?php 
	function get_unapproved_pics()
	{
		$approved = 0;
		$query = "SELECT * FROM pics WHERE approved = ' $approved'";
		$queryrun =  $GLOBALS['$conn']->query($query);
		if($queryrun->num_rows > 0)
		{
			while ($row = $queryrun->fetch_assoc()) {
				$pid = $row['pid'];
				$picname = $row['picname'];
				$uname = $row['username'];
				$src = 'uploads/'.$uname.'/'.$picname;
				?>
				<div id="row-<?php echo $pid ; ?>">
					<div class="col-md-4">
						<img src="<?php echo $src ;?>" id="<?php echo $pid ; ?>">
					</div>
					<div class="col-md-4">
						<?php echo $picname; ?>
					</div>
					<div class="col-md-4">
						<button  id="Yes-<?php echo $pid;?>;" onclick=approvedimage(<?php echo $pid ;?>)>Yes</button>
						<button id="No-<?php echo $pid;?>;" onclick=deleteimage(<?php echo $pid ;?>)>No</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<?php
			}
		}
	}			
				 ?>
			</div>
		</div>
	</div>
</section>