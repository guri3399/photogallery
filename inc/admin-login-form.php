<section class="admin-login">
	<div class="section-header center">
		<h1>Login</h1>
		<h6><a href="index.php">Home</a> &gt; <span>Admin-Login</span></h6>
	</div>
	<div class="container">
		<div class="row">
			<!-- here we are using post method for sending the data to the server  -->
			<!-- value="<?php // if(isset($_POST['fname'])){echo $_POST['fname'];}?>"  if any validation false means user didnt provided valid email address then we are restoring the values using this raher a error is occured vallues filled by user will be as it is -->
			<form method="POST" id="admin-login-form" action="admin.php">
				<input type="text" name="username" id="username" placeholder="Choose a username" value="<?php if(isset($_POST['username'])){echo $_POST['username'];}?>">
				<input type="password" name="password" id="password" placeholder="Enter Your Password Here">
				<input type="submit" id="submit-admin" name="submit" value="Login" class="primary-bg white" >
			</form>
			<div id="error">

				
			</div>
			<div id="success">
				
			</div>
		</div>
	</div>
</section>
<!-- 
check whether the sumbit button has been cliked or not 
we need  to check also for input fields
whether the input field is empty or not
whether the number of charchters exceeeds its permitted characters
if this is ok then we will store it in variables -->
<?php
	include 'db.inc.php';
	// intialisation of all varibles we needed
	$username = "";
	$password = "";
	$error = array( );  
	function login($username,$password)
	{
		$query = "SELECT * FROM admin ";
		$queryrun =  $GLOBALS['$conn']->query($query);
		if($queryrun->num_rows > 0)
		{
		        $_SESSION['admin'] = $username ;
		       
            //   header('Location:index.php');
            echo "logged in successfully";

			 ?>
		        	<script type="text/javascript">
		        		window.location.href="admin.php";
		        	</script>
		        <?php
        }
    }
	function sanitize($data) // we can use also php filters
	{
		$data = trim($data); 
		// The trim() function is used to remove the white spaces and other predefined characters from the left and right sides of a string.
		$data= stripcslashes($data);
		// The stripslashes() function removes backslashes added by the addslashes() function.

		// Tip: This function can be used to clean up data retrieved from a database or from an HTML form.


		$data = htmlspecialchars($data);
		// The htmlspecialchars function in PHP is used to convert 5 characters into corresponding HTML entities where applicable. It is used to encode user input on a website so that users cannot insert harmful HTML codes into a site. ENT_COMPAT is the default if quote_style is not specified.
		return $data;
	}
// validation 
	if (isset($_POST['submit'])) {
		$username = sanitize($_POST['username']);
		$password = sanitize($_POST['password']);
		if (empty($username)) 
		{
			?>
				<script type="text/javascript">
					$('#error').append("Enter username");
				</script>
			<?php
		}
		elseif(empty($password))
		{
			?>
				<script type="text/javascript">
					$('#error').append("Enter Password");
				</script>
			<?php
		}
		else
		{
			login($username,$password);
		}
	}
	else
	{
		echo "not gone in if";
	}
?>
