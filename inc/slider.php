<section class="slider center white">
	<!-- id particles id using particle js animation for understading it find its documention -->
	<!-- this is link for js plugin https://vincentgarreau.com/particles.js/ -->
	<!-- it use canvas for displaying so i used canvas as block property -->
	<div id="particles"></div>
	<div class="slider-content">
		<h1>Welcome To <span class="bold primary"> PhotoGallery</span> </h1>

		<!-- this text rotator class is defined in simletextrotaotor.css check it in this file  -->
		<!-- for full information you can check here https://github.com/peachananr/simple-text-rotator -->
		<p class="text-rotator blue"><span class="rotate">Upload,Share,Enjoy</span></p>
		<!-- we are generating content as a border using after pseudo class  -->
		<p class="call-to-action"><a href="#" class="cta">VIEW GALLERY</a></p>
	</div>
</section>