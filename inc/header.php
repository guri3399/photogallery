<!-- here we will check whether we have any session whether the user has already logged in or not. 
1...If a user is not logged in that means user is a guest.. For guest we will provide login/register in navbar and logou will be hidden.. 
2..If a user is Logged in the login/register will be hidden , we will only show logout in navbar -->
<?php
    session_start();
    // $_SESSION['user'] = "ME";
    // in this line we are providng value to session which means if (null) will not work and else will bw work we will see logout only
   
    if(isset($_SESSION['user']))
    {
        $session = $_SESSION['user'];
    }
    else
    {
        $session = null;
    }
    // $adminSession = NULL;
    // if(isset($_SESSION['admin']))
    // {
    //     $adminSession = $_SESSION['admin'];
    // }

    // else
    // {
    //     $adminSession = NULL;
    // }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,700;0,900;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/simpletextrotator.css">
    <link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <script  type="text/javascript" src="js/jquery.js"></script>
</head>
<body>
   <header>
    <div class="container">
        <div class="row">
          <!--   class pull left is working as float lweft it will pull itself to totally left and pull right is also same it is pulling the content to right forcefully -->
            <a href="" class="logo pull-left bold">Photo<span class="primary">G</span>allery</a>
            <nav class="pull-right right">
                <button type="button" class="btn green-bg" data-toggle="collapse" data-target="#menu">
                  <i class="fa fa-navicon"></i>
                </button>
                <div class="collapse" id="menu">
                    <ul class="center">
                        <li><a href="">Home</a></li>
                        <li><a href="">Gallery</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <?php 

                            if($session==null)
                            {
                        ?>
                            <li class="login"><a href="" class="blue-bg">Login</a></li>
                            <li class="register"> <a href="" class="primary-bg">Register</a></li>
                        <?php
                            }
                            else
                            {
                        ?>
                            <li class="logout"><a href="#" class="primary-bg">Logout</a></li>
                        <?php
                            }
                        ?>                   
                    </ul>
                </div>
            </nav>
        </div>
    </div>
   </header>
