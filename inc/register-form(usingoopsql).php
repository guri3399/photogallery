<section class="registration">
	<div class="section-header center">
		<h1>Registration</h1>
		<h6><a href="index.php">Home</a> &gt; <span>Register</span></h6>
	</div>
	<div class="container">
		<div class="row">
			<!-- here we are using post method for sending the data to the server  -->
			<form method="POST" id="register-form" action="register.php">
				<input type="text" name="fname" id="fname" placeholder="Enter First Name">
				<input type="text" name="lname" id="lname" placeholder="Enter Last Name">
				<input type="text" name="username" id="username" placeholder="Choose a username">
				<input type="email" name="email" id="email" placeholder="Enter Your Email Address">
				<input type="password" name="password" id="password" placeholder="Enter Your Password Here">
				<input type="password" name="confirm-password" id="confirm-password" placeholder="Confirm Your Password Here">
				<textarea name="bio" id="bio" placeholder="Enter Your Bio Here(Optional)"></textarea>
				<input type="submit" id="submit" name="submit" value="Register" class="primary-bg white">
			</form>
			<div id="error">

				
			</div>
			<div id="success">
				
			</div>
		</div>
	</div>
</section>
<!-- 
check whether the sumbit button has been cliked or not 
we need  to check also for input fields
whether the input field is empty or not
whether the number of charchters exceeeds its permitted characters
if this is ok then we will store it in variables -->
<?php
	// include 'db.inc.php';
	$GLOBALS['$servername'] = "localhost";
	$GLOBALS['$username'] = "root";
	$GLOBALS['$password'] = "";
	$GLOBALS['$dbname'] = "imagegallery";
	$GLOBALS['$conn'] = new mysqli($GLOBALS['$servername'],$GLOBALS['$username'],$GLOBALS['$password'],$GLOBALS['$dbname']);
	if ($GLOBALS['$conn']->connect_error) {
    die("Connection failed: " .  $GLOBALS['$conn']->connect_error);
	}
	echo "Connected successfully";
	// intialisation of all varibles we needed
	$fname = "";
	$lname = "";
	$username = "";
	$password = "";
	$email = "";
	$bio = "";
	$uploads =0 ;
	$id ="";
	$error = array( );  
	function register($id,$fname,$lname,$username,$email,$password,$bio,$uploads)
	{
		$newpwd = md5($password); //encrypting the password
		$query = "INSERT INTO user VALUES('$id','$fname','$lname','$username','$email','$newpwd','$bio','$uploads')";
		if($GLOBALS['$conn']->query($query) === TRUE)
		{
			?>
				<script type="text/javascript">
					$('#success').append("You Have Been Registered Successfully<a href='login.php'>Click Here To Login </a>");
				</script>
			<?php
		}
		else
		{
			?>
				<script type="text/javascript">
					$('#error').append("Error Registering");
				</script>
			<?php
		}
	}
	function sanitize($data) // we can use also php filters
	{
		$data = trim($data); 
		// The trim() function is used to remove the white spaces and other predefined characters from the left and right sides of a string.
		$data= stripcslashes($data);
		// The stripslashes() function removes backslashes added by the addslashes() function.

		// Tip: This function can be used to clean up data retrieved from a database or from an HTML form.


		$data= htmlspecialchars($data);
		// The htmlspecialchars function in PHP is used to convert 5 characters into corresponding HTML entities where applicable. It is used to encode user input on a website so that users cannot insert harmful HTML codes into a site. ENT_COMPAT is the default if quote_style is not specified.
		return $data;
	}
// validation 
	if (isset($_POST['submit'])) {
		
		// 1.we are checking in first if() that input field is empty or not
		// so if empty we will alert a error that it is reqquired field
		// 2. we are checking number of charchters in it using strlen() of fname if its greater than (length defined in databse for accepting value) then also we will show error that it should have the max limit of charchters 
		// 3. if all these false then we can store values in variables...

		// this code validating first name
		if (empty($_POST['fname'])) 
		{
			$error[] = "First Name Required";
		}
		else if(strlen($_POST['fname'])>25)
		{
			$error[] = "First Name Should Have a Maximum Of 25 characters";
		}
		else
		{
			$fname = sanitize($_POST['fname']);
		}

		// this code validating last name
		if (empty($_POST['lname'])) 
		{
			$error[] = "Last Name Required";
		}
		else if(strlen($_POST['lname'])>25)
		{
			$error[] = "Last Name Should Have a Maximum Of 25 characters";
		}
		else
		{
			$lname = sanitize($_POST['lname']);
		}
		// this code validating user name
		if (empty($_POST['username']))
		{
			$error[] = "UserName Required";
		}
		else if(strlen($_POST['username'])>25)
		{
			$error[] = "UserName Should Have a Maximum Of 25 characters";
		}
		else
		{
			$username = sanitize($_POST['username']);
		}
		// this code validating email
		if (empty($_POST['email'])) {
			$error[] = "Email Required";
		}
		else if(strlen($_POST['email'])>50)
		{
			$error[] = "Email Should Have a Maximum Of 50 characters";
		}
		// checking its valid email or not (its of email type or not)
		// you can see all details here https://www.w3schools.com/php/php_ref_filter.asp
		// else if not validated then just add this one to error array
		elseif (!(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)))
		{
			$error[] = "Email Is Not a Valid Email Address";
		}
		else
		{
			$email = sanitize($_POST['email']);
		}
		// this code validating password
		if (empty($_POST['password'])) 
		{
			$error[] = "Password Required";
		}
		else if(strlen($_POST['password'])>32)
		{
			$error[] = "Password Should Have a Maximum Of 32 characters";
		}
		else
		{
			$password = sanitize($_POST['lname']);
			// confirm password is same or not
			if (!empty($_POST['confirm-password']))
			{
				if ($_POST['password']!=$_POST['confirm-password'])
				{
					$error[] = "Password And Confirm Passwords Are Not Matching";
				}
			}
			else
			{
				$error[] = 'Confirm Your Password';
			}
		}
		if (!empty($bio)) 
		{
			$bio = sanitize($_POST['bio']);
		}
		// this funtion is checking if no error then what to do
		if (count($error)==0) 
		{
			// $conn = new mysqli($servername, $username, $password, $dbname);
			$checkusername = "SELECT * FROM user WHERE username = '$username'";
			$runqueryusername = $GLOBALS['$conn']->query($checkusername);

			$checkemail = "SELECT * FROM user WHERE email = '$email'";
			$runqueryemail = $GLOBALS['$conn']->query($checkemail);

			if ($runqueryusername->num_rows > 0 ) 
			{
				?>
				<script type="text/javascript">
					$('#error').append("<?php echo "UserName Exists"; ?>");
				</script>
				<?php
			}

			elseif ($runqueryusername->num_rows > 0 ) {
				?>
				<script type="text/javascript">
					$('#error').append("<?php echo "Email Exists"; ?>");
				</script>
				<?php
			}
			else
			{
				register($id,$fname,$lname,$username,$email,$password,$bio,$uploads);
				// echo "values will be submitted to the server";
			}
		}

		// as if its here it means there are errors and we are converting array to string and then storing it in value varible and displaying the error
		else
		{
			foreach ($error as $key => $value)
			{
				?>
				<script type="text/javascript">
					$('#error').append("<?php echo $value . '<br>'; ?>");
				</script>
				<?php
			}
		}
	}
?>